**Specifications for Piscary home page.**

Version 0.0, 26 September 2013.

Copyright (c) 2013 Peter Harpending. <<pharpend2@gmail.com>>

Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.  This file is offered as-is, without any
warranty.

--

I am working with the assumption that we will use the Bootstrap 3 CSS
Library. With that in mind, I will probably base my design largely off
of the
[bootstrap examples](http://getbootstrap.com/getting-started/#examples).

* The website must have a copy of the club charter, as well as a method
  of contacting everybody.
* The website must have a link to the code for the website.

You know what, screw it. I really like the
[Jumbotron layout](http://getbootstrap.com/examples/jumbotron/) on
Bootstrap, with a couple of provisions:

* I want to adapt it such that there exist more than three subsequent
  articles - perhaps up to nine.
* At that point, the layout should produce a "page navigator" at the
  bottom of the "page". Going to the next "page" will cycle through the
  next nine articles at the bottom of the page, whilst leaving the top
  section of the page intact.
  + If JavaScript is disabled, this feature will obviously not work. The
    solution is to write the feature entirely in JavaScript. This way,
    no extraneous formatting will exist if JavaScript is disabled.
* There will always be a link to the archives, so JavaScript haters can
  still access old articles.

The site must be fully functional without JavaScript. That is to say,
JavaScript haters can still use the site pain-free. Too many sites rely
on JavaScript for basic functionality.

This isn't to say that we can't add fancy things in JavaScript. On the
contrary, I am saying that we *only* add fancy things in JavaScript.
