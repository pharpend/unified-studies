**SCSS standards for Piscary web project**

Version 0.1, 14 November 2013

Copyright (c) 2013 Peter Harpending. <<pharpend2@gmail.com>>

Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.  This file is offered as-is, without any
warranty.

## Introduction

This is a draft of the coding standards for SCSS. SCSS is a superset
of CSS, so these standards apply to CSS as well. 

## Naming conventions

Unless there is a good reason not to (read: Bootstrap), all of our names will
follow the `lower-case-with-hyphens` convention. 

## Indentation

All indents are two spaces. If you don't like this, too bad.

## Braces

Opening braces start on the same line as the block definition. Closing braces go
on their own line.

Every closing brace, or other such parenthetical, closing a block of or
exceeding five lines in length, or whose block nests another braced,
angle-bracketed, or otherwise parenthesized block, must immediately be followed
by a comment indicating what the brace, bracket, or other parenthetical was
closing.

## Comments

Use comments liberally to explain what you are doing in your
code. Favor one-line comments (`// ...`) over block comments (`/*
... */`). Block comments are much harder to read, and much more likely
to be abused.


Use your editor's commenting function to comment out large blocks of
code. 

Suppose you need to comment out this block of code.

```scss
this {
  &.is-some {
    code: it-needs-to-be-commented-out;
    do-so-with: editor-function;
  }// is-some
}// this
```

This is how you do it with your editor's comment function.

```scss
// this {
//   &.is-some {
//     code: it-needs-to-be-commented-out;
//     do-so-with: editor-function;
//   }// is-some
// }// this
```

This is how you would probably try to do it manually.

```scss
/*this {
  &.is-some {
    code: it-needs-to-be-commented-out;
    do-so-with: editor-function;
  }// is-some
}// this*/
```

That looks terrible. If you do that, you are a terrible person. In
fact, if you try to push any commits with manually-commented code, you
can consider it the end of your involvement in the project.
