This document outlines the coding standards for HTML code in the
Piscary Website project.

Version 0.1, 14 November 2013

Copyright (c) 2013 Peter Harpending. <<pharpend2@gmail.com>>

Copying and distribution of this file, with or without modification,
are permitted in any medium without royalty provided the copyright
notice and this notice are preserved.  This file is offered as-is,
without any warranty.

# Skeleton stuff

All HTML files need to be based on the HTML skeleton in the `skel/`
directory in this repository. 

All HTML files are written in HTML 5. IE users can switch to a better
browser if they want a better experience.

# Cleanliness

## Line length

You must follow the 80-character rule. That means, no lines longer
than 80 characters. If you have an egregiously long line, wrap it up
like this.

```html
<!-- no -->
<some-obscene-tag parameter="some obscenely long string." another-parameter="some other string.">

<!-- ideal-->
<some-obscene-tag parameter="some obscenely long string."
                  another-parameter="some other string.">

<!-- also acceptable; use more than one indent for parameters -->
<some-obscene-tag
    parameter="some obscenely long string."
    another-parameter="some other string.">

<!-- This is not acceptable, however, because the parameters could -->
<!-- easily be confused with the text inside of a subsequent block.-->
<some-obscene-tag
  parameter="some obscenely long string."
  another-parameter="some other string.">
```

## Indents

Indents are two spaces. Yes, I know this conflicts with four-space
indents in PHP. If you have a half-decent editor (i.e. **not**
Substandard Text 2), then you should be fine.

## Comments

There is only one comment syntax in HTML. Use it on a line-per-line
basis, rather than block-commenting a region. This makes it much
easier to *un*comment said region.

```html
<head>
  <title>Some dumb title</title>
  <link rel="I dont care" href="narstarstart">

  <meta text="This is some text. This is not actual HTML code.">
  <meta text="um... um... the... um... meta tags">
</head>
```

Properly commented: 
```html
