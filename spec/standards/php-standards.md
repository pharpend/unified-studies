**PHP standards for Piscary web project**

Version 0.0, 3 October 2013.

Copyright (c) 2013 Peter Harpending. <<pharpend2@gmail.com>>

Copying and distribution of this file, with or without modification, are
permitted in any medium without royalty provided the copyright notice
and this notice are preserved.  This file is offered as-is, without any
warranty.

## Introduction

This is a rough draft of our coding standards for PHP. So far, it covers
Naming conventions, white space, and brace placement/commenting.

I use the terms "function" and "method" interchangably. Deal with it.

## Naming conventions

Variables will follow the `$lower_case_with_underscores`
convention. Functions will follow the `lower_case_with_underscores()`
convention.

## White space

This is pretty simple - use white space when it provides for more
readability, and avoid it when it makes the code less readable.

### Indentation

All indents will be four spaces.

```php
function some_function() {
    // some code...
}// end of some_function()
```

### Hanging whitespace

If you need to extend a function signature arguments over multiple
lines:

```php
// Yes
function f($the_first_obnoxiously_elongated_variable_name,
           $the_second_obnoxiously_elongated_variable_name) {
    // some code...       
}// end of f()

// No
function g($the_first_obnoxiously_elongated_variable_name,   
    $the_second_obnoxiously_elongated_variable_name) {
    // some code...
}

// Hell no
function h($the_first_obnoxiously_elongated_variable_name, $the_second_obnoxiously_elongated_variable_name) {
    // some code...
}
```

If you need to extend a function call arguments over multiple lines:

```php
// No
funcall($the_first_obnoxiously_elongated_variable_name,
$the_second_obnoxiously_elongated_variable_name)

// No
funcall($the_first_obnoxiously_elongated_variable_name, $the_second_obnoxiously_elongated_variable_name)

// This works
funcall($the_first_obnoxiously_elongated_variable_name,
    $the_second_obnoxiously_elongated_variable_name)
    
// But this provides less ambiguity
funcall($the_first_obnoxiously_elongated_variable_name,
        $the_second_obnoxiously_elongated_variable_name)
```

The same applies to associative arrays; or, any arrays for that matter.

### Things that get you kicked

```php
funcall ( $the_first_argument, $the_second_argument )
```

### Code blocks

The opening brace of the code block goes on the same line as the
terminating line of the body definition.

```php
// No
for($i=0; $i<10; $i++)
{
    // some code...
}// end for

// No, and why?
for($i=0; $i<10; $i++)
  {
    // some code...
  }// end for

// Yes
for($i=0; $i<10; $i++) {
    // some code...
}// end for
```

Closing braces go on their own line, followed by a comment (if the body
is more than 5 lines in length).

Every closing brace, or any parenthetical for that matter (`)`, `]`,
`}`) closing a body of more than 5 lines in length must be immediately
followed by a comment indicating what the brace was closing.

```php
for($i=0; $i<10; $i++) {
    ...
}// end for

function some_function() {
    ...
}// end of some_function()

$x = array(
    ...
)// array
```
